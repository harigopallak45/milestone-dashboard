import { shallowMount } from "@vue/test-utils";
import DashboardCounters from "@/components/dashboard_counters.vue";
import ProgressRing from "@/components/progress_ring.vue";
import generateIssues from "../../utils/generate_issues";

let wrapper;
const issueLength = 10;
const defaultProps = {
  milestone: "13.0",
  issues: generateIssues(issueLength),
  todos: [{ id: 0 }, { id: 1 }],
};

// This only work when issue length is <= 10
// But it's good enough for this test
const ISSUE_COMPLETION = (issueLength - 1) * 5;

// Hard-coding this because I know how many are in 13.0
const DAYS_IN_MILESTONE = 20;

// We mock the current date so these should be static
const DAYS_REMAINING_IN_MILESTONE = 11;
const DAYS_PASSED_IN_MILESTONE = 20;

describe("Dashboard Counter", () => {
  const RealDate = Date.now;

  beforeAll(() => {
    global.Date.now = jest.fn(() => new Date("2020-05-01T00:00:00Z").getTime());
  });

  afterAll(() => {
    global.Date.now = RealDate;
  });

  beforeEach(() => {
    wrapper = shallowMount(DashboardCounters, {
      propsData: defaultProps,
    });
  });

  afterEach(() => {
    wrapper.destroy();
  });

  it("should render the milestone", () => {
    expect(wrapper.find(".milestone").text()).toEqual(defaultProps.milestone);
  });

  it("should render the todos", () => {
    expect(wrapper.find(".todos").props().count).toEqual(
      defaultProps.todos.length
    );
  });

  it("should render the remaining issues", () => {
    expect(wrapper.find(".issues").props().count).toEqual(
      defaultProps.issues.length
    );
  });

  it("should render the remaining days", () => {
    expect(wrapper.find(".days").props().count).toEqual(
      DAYS_REMAINING_IN_MILESTONE
    );
  });

  it("should pass the issue completion to the progress ring", () => {
    expect(wrapper.find(ProgressRing).props().issueCompletion).toEqual(
      ISSUE_COMPLETION
    );
  });

  it("should pass the days in the milestone to the progress ring", () => {
    expect(wrapper.find(ProgressRing).props().daysInMilestone).toEqual(
      DAYS_IN_MILESTONE
    );
  });

  it("should pass the days passed in the milestone to the progress ring", () => {
    expect(wrapper.find(ProgressRing).props().daysInMilestone).toEqual(
      DAYS_PASSED_IN_MILESTONE
    );
  });
});
