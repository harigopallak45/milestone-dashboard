import { shallowMount } from "@vue/test-utils";
import IssueList from "@/components/issue_list.vue";
import IssueBody from "@/components/issue_body.vue";
import generateIssues from "../../utils/generate_issues";

describe("Issue List", () => {
  it("renders the correct amount of issues", () => {
    const issueAmount = 2;
    const wrapper = shallowMount(IssueList, {
      propsData: { issues: generateIssues(issueAmount) },
    });

    expect(wrapper.findAll(IssueBody).length).toEqual(issueAmount);
  });
});
