import defaultLabels from "./filler_labels";

export default (index, options = {}) => ({
  iid: `${index}`,
  title: `Issue ${index}`,
  completion: (index * 10) % 100,
  labels: { nodes: defaultLabels },
  webUrl: "https://gitlab.com",
  ...options,
});
