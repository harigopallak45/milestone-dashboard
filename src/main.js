import Vue from "vue";
import App from "./main_app.vue";
import router from "./router";
import "./register_service_worker";
import { createProvider } from "./vue_apollo";
import "./assets/tailwind.css";

Vue.config.productionTip = false;

new Vue({
  router,
  apolloProvider: createProvider(),
  render: (h) => h(App),
}).$mount("#app");
