import getMilestoneEndDate from "@/utils/get_milestone_end_date";

export default (milestone) => {
  let [major, minor] = milestone.split(".").map((m) => parseInt(m, 10));

  major = minor ? major : major - 1;
  minor = (minor + 11) % 12;

  return getMilestoneEndDate(`${major}.${minor}`);
};
