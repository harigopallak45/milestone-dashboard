import getWorkflowLabel from "@/utils/get_workflow_label";
import {
  WORKFLOW_IN_DEV,
  WORKFLOW_REVIEW,
  WORKFLOW_VERIFICATION,
} from "@/constants";

export default (issue = {}) => {
  if (issue.state === "closed") {
    return 100;
  }

  const workflowLabel = getWorkflowLabel(issue.labels.nodes);

  // TODO: Get fancier with days elapsed using frontend weight.
  switch (workflowLabel) {
    case WORKFLOW_IN_DEV:
      return 20;
    case WORKFLOW_REVIEW:
      return 60;
    case WORKFLOW_VERIFICATION:
      return 90;
    default:
      return 0;
  }
};
