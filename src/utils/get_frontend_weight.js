export default (labels = []) => {
  const weightLabel = labels.find(({ title }) =>
    title.match(/^frontend-weight::/)
  );
  if (!weightLabel) {
    return 0;
  }

  const [suffix] = weightLabel.title.match(/(?<=::).+$/);

  if (Number.isNaN(Number(suffix))) {
    return 0;
  }

  const number = parseInt(suffix, 10);

  switch (number) {
    case 1:
      return 0.5;
    case 2:
      return 1;
    case 3:
      return 3;
    case 5:
      return 5;
    case 8:
      return 13;
    case 13:
      return 20;
    default:
      return number;
  }
};
